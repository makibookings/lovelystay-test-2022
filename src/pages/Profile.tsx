import React, { useEffect, useState } from 'react';
import { Link, useNavigate, useParams } from "react-router-dom";
import { gitUser } from "../interfaces/gitUser";
import axios from "axios";
import { API_LINK } from "../types/types";


function Profile() {
    {/**
     @gitUser - setup a interface for the object data userProfile
     @userProfileRepos - didn't use a custom interface here just for time saving reasons
     **/}
    const { name } = useParams();
    const navigate = useNavigate();
    const [userProfile,setUserProfile] = useState<gitUser>();
    const [userProfileRepos,setUserProfileRepos] = useState([] as any);

    const fetchGitUser = async (name: string | undefined) => {
        try {
            const response = await axios.get(API_LINK + `/users/${name}`);
            setUserProfile(response.data);
            return true;
        } catch (e) {
            console.error({e});
        }

    }
    const fetchGitUserRepos = async (name: string | undefined) => {
        try {
            const response = await axios.get(API_LINK + `/orgs/${name}/repos`);
            setUserProfileRepos(response.data);
            return true;
        } catch (e) {
            console.error({e});
        }

    }

    {/**
     Must likely would have been better here to wrap everything on a Promise.all and concat all data into a profile obj or just use redux
     **/}
    useEffect(() => {
        try {
            fetchGitUser(name).then((r) => (console.log(r)));
            fetchGitUserRepos(name).then((r) => (console.log(r)));
        } catch (error) {
            navigate('/');
        }
    }, [])

    return (
        <>
            <nav>
                <Link to="/">Back to Search</Link>
            </nav>

            {userProfile ? (
                    <div className={"card"}>
                        <img src={userProfile.avatar_url} alt=""/>
                        <span className="info">
                             <h2> {userProfile.name} </h2>
                             <h4> Total Number of Repos {userProfile.public_repos} </h4>
                        </span>
                    </div>
            ) : (<></>)}


            <ul className={"repo-list"}>
                {userProfileRepos && userProfileRepos.map(({ id, name, description } : { id:number, name:string, description:string }) => (
                    <li key={id}>
                        <h3>{name}</h3>
                        <p>
                            {description}
                        </p>
                    </li>
                ))}
            </ul>

        </>
    );
}

export default Profile;
