import React, {  useState } from 'react';
import { useNavigate } from "react-router-dom";
import axios from "axios";
import {API_LINK} from "../types/types";


function Search() {
    {/**
     @useNavigation - to handle navigation between pages after form submit
     @gitUserSearch - Current state type string of the input form
     @isDisabled - Disable submit button on form submit
     @isError - Handle usernames not found in github.api
     **/}
    const navigate = useNavigate();
    const [gitUserSearch, setGitUserSearch] = useState<string>("");
    const [isDisabled, setDisabled] = useState(false);
    const [isError, setIsError] = useState(false);


    {/**
      @ReactQuery - didn't think i would need to cache the date here since its a search form, and it would variate often
      @navigate - dind't want to complicate with redux, so just passing the name throu
     navigation and making a new request for the rest of the info on the profile page, other approach would be to save the profile info
     into a global state with redux and then trigger the repos fetch inside the profile page with a new action.
     **/}
    const searchForGitUser = async (event: any) => {
        event.preventDefault();
        setDisabled(true);
        try {
            const response = await axios.get(API_LINK + `/users/${gitUserSearch}`);
            navigate('/profile/' + response.data.login);
            setDisabled(false);
            setIsError(false);
        } catch (error) {
            console.error(error);
            setDisabled(false);
            setIsError(true);

        }
    }

    return (
        <>
            <form className={"form-search"} onSubmit={searchForGitUser}>
                <input value={gitUserSearch} onChange={event => setGitUserSearch(event.target.value)} placeholder="Enter a username..." type={"text"}/>
                <button disabled={isDisabled}>Search</button>
            </form>

            {isError ? (
                <>
                   <h1>User not found - try again</h1>
                </>
            ) : (<></>) }
        </>
      );
}

export default Search;
