import React from 'react';
import logo from './logo.svg';
import './App.css';
import { Routes, Route, Link } from "react-router-dom";
import Search from "./pages/Search";
import Profile from "./pages/Profile";
import {
    useQuery,
    QueryClient,
    QueryClientProvider,
} from 'react-query';
const queryClient = new QueryClient()


{/**
    @ReactQuery - Cache future fetched data
    @ReactNavigation - Handling navigation between screens
 **/}

function App() {
  return (
    <>
        <QueryClientProvider client={queryClient}>
              <Routes>
                <Route path="/" element={<Search />} />
                <Route path="profile/:name" element={<Profile />} />
              </Routes>
        </QueryClientProvider>
    </>
  );
}

export default App;
