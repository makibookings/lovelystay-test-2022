# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app) in my Webstorm which i believe some package aren't the most recent.

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

## Tests

There isn't tests as i mention on the interview i don't have experience running unit test, 
however am still learning it but didn't think the tests i currently know, would make a difference 
on this project as am still learning userEvent clicks and fireEvents to test the passing data, 
write tests just to check if the components are rendering didn't seem much.


## Description

Please read code comments as i explain more of the process i did use
